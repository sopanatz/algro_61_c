cmake_minimum_required(VERSION 3.13)
project(Arrat01 C)

set(CMAKE_C_STANDARD 99)

add_executable(Arrat01 main.c)